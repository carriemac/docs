---
title: Parts of the Chronologue Time Travel Telescope
draft: true
menu:
  docs:
    parent: "docs"
    identifier: "Parts of the Chronologue Time Travel Telescope"
weight: 20
---
Explains the parts that the telescope is made of.